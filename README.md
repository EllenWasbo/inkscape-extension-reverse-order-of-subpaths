# Inkscape extension reverse order of subpaths

Reverse order of subpaths (combined paths) without changing the direction of each subpath.

When drawing paths and combining these the order of the subpaths is from the last to the first drawn. This extension reverse the order of the subpaths and thus the order in which the paths were drawn can be restored.

Download the .inx and .py file and place them in the local extension folder. Restart Inkscape and find the extension in Extensions > Cutlings > Reverse order of subpaths

Use together with the extension to [number subpaths](https://gitlab.com/EllenWasbo/inkscape-extension-number-subpaths) to visualize the effect of reversing the subpaths.
